char *s = N_("(C) 2014 Zvika Haramaty");
char *s = N_("ABCer");
char *s = N_("Active letters");
char *s = N_("Give me a word!");
char *s = N_("Max num of consonants:");
char *s = N_("Max num of letters:");
char *s = N_("Max num of vowels:");
char *s = N_("Report a bug");
char *s = N_("Show letters");
char *s = N_("Simple application to help kids learning the ABC");
char *s = N_("WHAT?\n"
             "=====\n"
             "This application is aimed to help kids learning the ABC.\n"
             "It will randomly give them fake 'words' to read, and the user can control the complexity of the given words.\n"
             "\n"
             "HOW?\n"
             "====\n"
             "Initially, the user is given a single consonant (B) and a signle vowel (A).\n"
             "When the 'Give me a word!' button is clicked, the user will get a random word composed of a single letter.\n"
             "When appropriate, it's easy to add more consonants, more vowels and more letters.\n"
             "\n"
             "FAQ\n"
             "===\n"
             "1. I got less consonants/vowels/letters than I chose?\n"
             "- Pay attention, it's the 'Max num of' - the real number will be randomized up to this limit.\n"
             "\n"
             "2. How come there are 8 vowels, it should be 5?\n"
             "- For the training, I consider 'ea', 'ee' and 'oo' as a single vowel.\n"
             "\n"
             "3. How come there are 24 vowels, it should be 19?\n"
             "- For the training, I consider 'th', 'sh', etc. as a single consonant.\n"
             "\n"
             "4. I requested up to 4 letters, but I got a word of 5 letters?\n"
             "- According to questions 2 & 3, the word 'teeth' is only 3 letters 't', 'ee' and 'th'\n"
             "\n"
             "5. I found a bug, how can I report it?\n"
             "- Help menu/report a bug, or just go to https://bitbucket.org/haramaty/abcer/issues/new\n"
             "  There is no need to log in or register in order to report a bug, but it'll be nice to know who's reporting.\n"
             "  \n"
             " Special thanks\n"
             " ==============\n"
             " To my brother, Zachy, who developed similar program on our ZX Spectrum when I learnt the ABC...\n"
             "");
char *s = N_("What's going on?");
char *s = N_("_Action");
char *s = N_("_Help");
char *s = N_("_Language");
char *s = N_("gtk-about");
char *s = N_("gtk-quit");
