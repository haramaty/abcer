#!/usr/bin/env python
# Trying to implement the GUI with GLADE3 & GTK2

import abcer_core
import sys
import os
import json
import webbrowser
import gtk
import gettext
from xml_translator import translate_xml


# copied from http://stackoverflow.com/questions/7674790/bundling-data-files-with-pyinstaller-onefile/13790741#13790741
# allows
def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


APP_NAME = "abcer"

config = {'version': 1.0, 'lang': 'en_US', 'spins': (1, 1, 1)}

local_path = resource_path('.')

def set_language(name):
    # this command is not standard, but I had to include to 'name' in the 'domain'
    # in order to have different names for the resulting '.mo' files
    # because of some bug in PyInstaller being confused of 2 files with the same name
    lang = gettext.translation(APP_NAME + "_" + name, local_path, languages=[name])
    lang.install(unicode = True)

    rtl_languages = ('he_IL')

    if name in rtl_languages:
        gtk.widget_set_default_direction(gtk.TEXT_DIR_RTL)
    else:
        gtk.widget_set_default_direction(gtk.TEXT_DIR_LTR)



configFileName = "ABCer.cfg"
def getConfigFilePath():
    return os.path.join(os.path.expanduser("~"), configFileName)


class GladeGTK:
    def getSpinsValues(self):
        spins = ["consonantsSpin", "vowelsSpin", "lettersSpin"]
        return tuple([self.builder.get_object(spin).get_value() for spin in spins])

    def setSpinsValues(self, values):
        spins = ["consonantsSpin", "vowelsSpin", "lettersSpin"]
        #return tuple([self.builder.get_object(spin).get_value() for spin in spins])
        for index, spin in enumerate(spins):
            self.builder.get_object(spin).set_value(values[index])

    def on_giveMeWordBtn_clicked(self, widget):
        (userMaxConsonants, userMaxVowels, userMaxLetters) = self.getSpinsValues()

        word = (abcer_core.get_rand_word(userMaxConsonants, userMaxVowels, userMaxLetters))
        #self.builder.get_object("resultsLabel").set_text("<b word >")
        label = self.builder.get_object("resultsLabel")
        label.set_markup('<span size="25000">%s</span>' % word)


        #label.set_use_markup(gtk.TRUE)
        # weights
        #label.set_markup('normal text, <b>bold text</b>, <i>italic text</i>')
        # size; 38000 = 38pt (use pt size * 1000)
        #label.set_markup('<span size="38000">big text</span>, normal text')

    def destroy(self, widget, data=None):
        self.quit()

    def quit(self):
        self.saveConfig()
        gtk.main_quit()

    def saveConfig(self):
        global config
        try:
            filePath = getConfigFilePath()
            f = open(filePath,'w')
            config['spins'] = self.getSpinsValues()
            f.write(json.dumps(config))
            f.close()
        except IOError:
            pass

    def readConfig(self):
        global config
        try:
            filePath = getConfigFilePath()
            f = open(filePath,'r')
            # if someday the 'version' field will be useful, I'd might want to change this
            # because it overrides our version with the version in file.
            config.update(json.loads(f.read()))

            f.close()
        except IOError:
            pass

    def showDialog(self, name):
        dialog = self.builder.get_object(name)
        dialog.run()
        dialog.hide()

    def on_showLettersMenuAction_activate(self, widget):
        #self.showDialog("showLettersDlg")
        dialog = self.builder.get_object("showLettersDlg")
        (userMaxConsonants, userMaxVowels, ignored) = self.getSpinsValues()
        consonants = abcer_core.get_active_consonants(int(userMaxConsonants))
        vowels = abcer_core.get_active_vowels(int(userMaxVowels))
        dialog.format_secondary_text(str(consonants) + "\n" + str(vowels))
        dialog.run()
        dialog.hide()

    def on_explainMenuAction_activate(self, widget):
        self.showDialog("explainDlg")

    def on_reportBugMenuAction_activate(self, widget):
        webbrowser.open("https://bitbucket.org/haramaty/abcer/issues/new",new=2)

    def on_aboutMenuAction_activate(self, widget):
        self.showDialog("aboutdialog1")

    def on_quitMenuAction_activate(self, widget):
        self.quit()

    def on_engLangItem_activate(self, widget):
        config['lang'] = 'en_US'
        self.restartWindow()

    def on_hebLangItem_activate(self, widget):
        config['lang'] = 'he_IL'
        self.restartWindow()

    def uri_hook_func(self, ignore1, url, ignore2):
        webbrowser.open(url, new=2)

    def restartWindow(self):
        self.saveConfig()
        self.window.hide()
        self.showWindow()

    def __init__(self):
        self.showWindow()

    def showWindow(self):
        self.readConfig()
        set_language(config['lang'])

        gtk.rc_add_default_file(resource_path("gtkrc"))

        #Set the Glade file
        self.builder = gtk.Builder()

        translated_glade = translate_xml(resource_path("abcer_main.glade"), _)
        self.builder.add_from_string(translated_glade)

        #Get the Main Window, and connect the "destroy" event
        self.window = self.builder.get_object("MainWindow")

        if (self.window):
            self.window.connect("destroy", self.destroy)

        consonantsSpin = self.builder.get_object("consonantsSpin")
        consonantsSpin.set_range(1, len(abcer_core.consonants))

        vowelsSpin = self.builder.get_object("vowelsSpin")
        vowelsSpin.set_range(1, len(abcer_core.vowels))

        self.setSpinsValues(config['spins'])

        self.builder.connect_signals(self)

        gtk.about_dialog_set_url_hook(self.uri_hook_func, data=None)

        self.window.show()


if __name__ == "__main__":
    GladeGTK()
    gtk.main()
