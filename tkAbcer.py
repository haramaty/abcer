# -*- coding: utf-8 -*-

''' GUI for abcer_core
'''

import abcer_core
from Tkinter import *
import tkFont
import ttk

def give_word(*args):
    resultsContents.set(abcer_core.get_rand_word(userMaxConsonants.get(), userMaxVowels.get(), userMaxLetters.get()))

root = Tk()
root.title("ABCer")

mainframe = ttk.Frame(root,  padding="3 3 12 12")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
mainframe.columnconfigure(0, weight=1)
mainframe.rowconfigure(0, weight=1)

#########################
####     LABELS      ####
#########################
label1 = ttk.Label(mainframe, text="Max num of consonants:").grid(column=2, row=1, sticky=(W, E))
label2 = ttk.Label(mainframe, text="Max num of vowels:").grid(column=2, row=2, sticky=(W, E))
label3 = ttk.Label(mainframe, text="Max num of letters:").grid(column=2, row=3, sticky=(W, E))

# spacers
ttk.Label(mainframe).grid(column=2, row=4, sticky=(W, E))
ttk.Label(mainframe).grid(column=2, row=9, sticky=(W, E))

ttk.Label(mainframe, text="החמודה לטל מוקדש").grid(column=3, row=10, sticky=(W, E))


#########################
####     COMBOS      ####
#########################
userMaxConsonants = IntVar()
userMaxConsonants.set(1)
ttk.Combobox(mainframe, values=range(1,len(abcer_core.consonants)+1),  textvariable=userMaxConsonants, width=2, state='readonly').grid(column=3, row=1)

userMaxVowels = IntVar()
userMaxVowels.set(1)
ttk.Combobox(mainframe, values=range(1,len(abcer_core.vowels)+1),  textvariable=userMaxVowels, width=2, state='readonly').grid(column=3, row=2)

userMaxLetters = IntVar()
userMaxLetters.set(1)
ttk.Combobox(mainframe, values=range(1,5+1),  textvariable=userMaxLetters, width=2, state='readonly').grid(column=3, row=3)


#########################
####     BUTTON      ####
#########################
#ttk.Button(mainframe, text="Give me word!", command=give_word, default='active' ).grid(column=3, row=4, sticky=W)
button = ttk.Button(mainframe, text="Give me a word!", command=give_word, default='active' )
button.grid(column=3, row=5, sticky=W)

root.bind('<Return>', give_word)


#########################
####     RESULT      ####
#########################
result_font =  tkFont.Font(family='Helvetica', size=18) #, weight='bold')

resultsContents = StringVar()
result_label = ttk.Label(mainframe, textvariable=resultsContents, font=result_font, relief='raised', width=6, anchor='center').grid(column=2, row=5, sticky=(W, E))


#########################
####     GENERAL     ####
#########################
for child in mainframe.winfo_children(): child.grid_configure(padx=5, pady=2)
root.mainloop()

# def calculate(*args):
#     try:
#         value = float(feet.get())
#         meters.set((0.3048 * value * 10000.0 + 0.5)/10000.0)
#     except ValueError:
#         pass
#
#
# mainframe = ttk.Frame(root, padding="3 3 12 12")
# mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
# mainframe.columnconfigure(0, weight=1)
# mainframe.rowconfigure(0, weight=1)
#
# feet = StringVar()
# meters = StringVar()
#
# feet_entry = ttk.Entry(mainframe, width=7, textvariable=feet)
# feet_entry.grid(column=2, row=1, sticky=(W, E))
#
# ttk.Label(mainframe, textvariable=meters).grid(column=2, row=2, sticky=(W, E))
# ttk.Button(mainframe, text="Calculate", command=calculate).grid(column=3, row=3, sticky=W)
#
# ttk.Label(mainframe, text="feet").grid(column=3, row=1, sticky=W)
# ttk.Label(mainframe, text="is equivalent to").grid(column=1, row=2, sticky=E)
# ttk.Label(mainframe, text="meters").grid(column=3, row=2, sticky=W)
#
# for child in mainframe.winfo_children(): child.grid_configure(padx=5, pady=5)
#
# feet_entry.focus()
# root.bind('<Return>', calculate)
#
# root.mainloop()
#
