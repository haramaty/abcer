��          �      ,      �     �     �     �     �     �     �     �            0     \  O     �     �     �  	   �     �  V  �     5	     G	     M	     ]	     i	     �	     �	     �	     �	  '   �	  (   �	     
     $
     *
     /
     3
                                                   
            	               (C) 2014 Zvika Haramaty ABCer Active letters Give me a word! Max num of consonants: Max num of letters: Max num of vowels: Report a bug Show letters Simple application to help kids learning the ABC WHAT?
=====
This application is aimed to help kids learning the ABC.
It will randomly give them fake 'words' to read, and the user can control the complexity of the given words.

HOW?
====
Initially, the user is given a single consonant (B) and a signle vowel (A).
When the 'Give me a word!' button is clicked, the user will get a random word composed of a single letter.
When appropriate, it's easy to add more consonants, more vowels and more letters.

FAQ
===
1. I got less consonants/vowels/letters than I chose?
- Pay attention, it's the 'Max num of' - the real number will be randomized up to this limit.

2. How come there are 8 vowels, it should be 5?
- For the training, I consider 'ea', 'ee' and 'oo' as a single vowel.

3. How come there are 24 vowels, it should be 19?
- For the training, I consider 'th', 'sh', etc. as a single consonant.

4. I requested up to 4 letters, but I got a word of 5 letters?
- According to questions 2 & 3, the word 'teeth' is only 3 letters 't', 'ee' and 'th'

5. I found a bug, how can I report it?
- Help menu/report a bug, or just go to https://bitbucket.org/haramaty/abcer/issues/new
  There is no need to log in or register in order to report a bug, but it'll be nice to know who's reporting.
  
 Special thanks
 ==============
 To my brother, Zachy, who developed similar program on our ZX Spectrum when I learnt the ABC...
 What's going on? _Action _Help _Language gtk-quit Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-09-11 16:38+0300
PO-Revision-Date: 2014-09-10 18:49+0300
Last-Translator: <EMAIL@ADDRESS>
Language-Team: Hebrew
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=CP1255
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 2014, ����� ����� ABCer ������� ������� �� �� ����! ��' ������� �� �������: ��' ������� �� ������: ��' ������� �� ������: ���� �� ��� ���� ������ ����� ����� ����� ������ ����� �� � ABC ����� ����... 
���� ����� ���� �������.
 �� ���� ��? ����� ���� ��� �� 