echo This is not a script, but rather a recipe for manual running!
pause

rem 1. Extract from Glade
rem =====================
c:\perl\bin\perl C:\Python27\Lib\site-packages\gtk-2.0\runtime\bin\intltool-extract --type=gettext/glade abcer_main.glade

rem 2. Extract from Python code
rem ===========================
rem Either Pyton built in script:
rem python C:\Python27\Tools\i18n\pygettext.py C:\Users\zharamax\PycharmProjects\LearnGTK\gladeAbcer.py
rem
rem Or GNU gettext.exe, from http://www.cubicweb.org/blogentry/565331
"C:\Program Files (x86)\gettext\0.17\bin\xgettext.exe"  -k_ -kN_ gladeAbcer.py *.glade.h -o messages.pot

rem 2.5
rem TBD: there is some conufsion between he.po and he_IL.po files - currently duplicating; verify what's needed

rem 3. Make Hebrew & English .po files
C:\Python27\Lib\site-packages\gtk-2.0\runtime\bin\msginit.exe --input=messages.pot --locale=he_IL
C:\Python27\Lib\site-packages\gtk-2.0\runtime\bin\msginit.exe --input=messages.pot --locale=en_US

rem 4. Manually Edit the HEBREW .po file
rem optional - merge 2 files - change file names before (add _old and _new to he_IL.po and he.po)
C:\Python27\Lib\site-packages\gtk-2.0\runtime\bin\msgmerge.exe old_he_IL.po new_he.po > he_IL.po

rem 5. Compile the .po files into .mo 
C:\Python27\Lib\site-packages\gtk-2.0\runtime\bin\msgfmt.exe --output-file=he_IL/LC_MESSAGES/abcer_he_IL.mo he_IL.po
C:\Python27\Lib\site-packages\gtk-2.0\runtime\bin\msgfmt.exe --output-file=en_US/LC_MESSAGES/abcer_en_US.mo en_US.po

pause
