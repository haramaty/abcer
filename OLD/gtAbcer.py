import pygtk
pygtk.require('2.0')
import gtk

class MainWindow:

    def delete_event(self, widget, event, data=None):
        print "delete"
        return False

    def destroy(self, widget, data=None):
        print("destroy")
        gtk.main_quit()

    def __init__(self):
        # create a new window
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)

        self.window.connect("delete_event", self.delete_event)
        self.window.connect("destroy", self.destroy)

        self.window.set_border_width(10)

        self.vbox1 = gtk.VBox(False, 0)
        self.vbox1.show()
        self.window.add(self.vbox1)

        self.hboxes = []
        for i in range(1,5 + 1):
            hbox = gtk.HBox(False, 0)
            hbox.show()
            self.vbox1.pack_start(hbox, True, False, 0)
            self.hboxes.append(hbox)

            label = gtk.Label("Labale")
            label.show()
            hbox.pack_start(label, True, True, 0)

        self.execButton = gtk.Button("Give me a word!")
        self.hboxes[3].pack_start(self.execButton, True, True)
        self.execButton.show()


        self.window.show()

    def main(self):
        gtk.main()

class HelloWorld:

    # This is a callback function. The data arguments are ignored
    # in this example. More on callbacks below.
    def hello(self, widget, data=None):
        print "hi"

    def delete_event(self, widget, event, data=None):
        # If you return FALSE in the "delete_event" signal handler,
        # GTK will emit the "destroy" signal. Returning TRUE means
        # you don't want the window to be destroyed.
        # This is useful for popping up 'are you sure you want to quit?'
        # type dialogs.
        print "delete event occurred"

        # Change FALSE to TRUE and the main window will not be destroyed
        # with a "delete_event".
        return False

    def destroy(self, widget, data=None):
        print "destroy signal occurred"
        gtk.main_quit()

    def __init__(self):
        # create a new window
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)

        # When the window is given the "delete_event" signal (this is given
        # by the window manager, usually by the "close" option, or on the
        # titlebar), we ask it to call the delete_event () function
        # as defined above. The data passed to the callback
        # function is NULL and is ignored in the callback function.
        self.window.connect("delete_event", self.delete_event)

        # Here we connect the "destroy" event to a signal handler.
        # This event occurs when we call gtk_widget_destroy() on the window,
        # or if we return FALSE in the "delete_event" callback.
        self.window.connect("destroy", self.destroy)

        # Sets the border width of the window.
        self.window.set_border_width(10)

        # Creates a new button with the label "Hello World".
        self.button = gtk.Button("hello button")

        # When the button receives the "clicked" signal, it will call the
        # function hello() passing it None as its argument.  The hello()
        # function is defined above.
        self.button.connect("clicked", self.hello, None)

        # This will cause the window to be destroyed by calling
        # gtk_widget_destroy(window) when "clicked".  Again, the destroy
        # signal could come from here, or the window manager.

#        self.button.connect_object("clicked", gtk.Widget.destroy, self.window)

        #Zvika - trying to destroy the button - diffing 'connect_object' & 'connect'...
        self.button.connect("clicked", gtk.Widget.destroy)

        # This packs the button into the window (a GTK container).
        self.window.add(self.button)

        # The final step is to display this newly created widget.
        self.button.show()

        # and the window
        self.window.show()

    def main(self):
        # All PyGTK applications must have a gtk.main(). Control ends here
        # and waits for an event to occur (like a key press or mouse event).
        gtk.main()


if __name__ == "__main__":
    mainWindow = MainWindow()
    mainWindow.main()