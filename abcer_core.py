import random

vowels = ("a", "e", "i", "o", "u", "ea", "ee", "oo")
consonants = ('b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x' ,'y', 'z', 'sh' , 'ch' ,'th')

def get_active_consonants(max_consonant):
    return consonants[:max_consonant]

def get_active_vowels(max_vowel):
    return vowels[:max_vowel]

def get_rand_vowel(max_vowel):
    index = random.randrange(0, max_vowel)
    return vowels[index]

def get_rand_consonant(max_consonant):
    index = random.randrange(0, max_consonant)
    return consonants[index]

prev_word = None

def get_rand_word(max_consonant, max_vowel, max_num_of_letters):
    global prev_word
    result = ""
    num_of_letters = random.randrange(1, max_num_of_letters+1)
    is_it_vowel = random.randrange(0,2)
    for i in range (0, num_of_letters):
        if is_it_vowel:
            result += get_rand_vowel(max_vowel)
        else:
            result += get_rand_consonant(max_consonant)
        is_it_vowel = 1 - is_it_vowel

    capitalize_mode = random.randrange(0, 3)
    if capitalize_mode == 0:
        # only first letter is capitalized
        result = result.capitalize()
    elif capitalize_mode == 1:
        result = result.upper()
    # elif capitalize_mode == 2 : nothing to do, leave it lower case

    if result != prev_word:
        prev_word = result
        return result
    else:
        return get_rand_word(max_consonant, max_vowel, max_num_of_letters)



if __name__ == "__main__":
    print ("Single vowel, Single consonant, adding letters")
    for i in range (1, 10):
        print(get_rand_word(1,1,i))

    print ("\nSingle vowel, Adding consonant")
    for i in range (1, 10):
        print(get_rand_word(i,1,4))

    print ("\nAdding vowel, Single consonant")
    for i in range (1, 10):
        print(get_rand_word(1,i,4))

    print ("\nAdding vowel, Adding consonant")
    for i in range (1, 10):
        print(get_rand_word(i,i,4))
